<?php

/**
 * Created by PhpStorm.
 * User: Daamaagee
 * Date: 12.09.2017
 * Time: 22:26
 */
require_once("./models/Reservator.php");
require_once("./models/ReservatorApiPartner.php");

class getEvent {

	function reserveRandomTicket($eventId) {

		if ($eventId % 2 == 0)
		{
			$Reservator = new Reservator();

			echo '|Local| ';
			echo 'creating object for event#';
			echo $eventId . '<br>';

			echo '|Local| reserving ticket #';
			echo $ticketId = $Reservator->ticketId();
			echo $eventId . ' VIA PARTNER API CALL<br>';

			echo '|Local| orderId # ';
			echo $orderId = $Reservator->orderId();
			echo ' created<br>';

			echo '|Local|';
			echo 'Sending admin notification: Order #';
			echo $orderId;
			echo ' created for event #' . $eventId;
			echo ' created<br><br>';
		}
		else
		{
			$partnerApi = new ReservatorApiPartner();

			echo '|partnerApi| ';
			echo 'creating object for event#';
			echo $eventId . '<br>';

			echo '|partnerApi| reserving ticket #';
			echo $ticketId = $partnerApi->ticketId();
			echo $eventId . '<br>';

			echo '|partnerApi| orderId # ';
			echo $orderId = $partnerApi->orderId();
			echo ' created<br>';

			echo '|partnerApi|';
			echo 'Sending admin notification: Order #';
			echo $orderId;
			echo ' created for event #' . $eventId;
			echo ' created<br><br>';
		}
	}
}